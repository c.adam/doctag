# docsrv_api.model.SignatureInputs

## Load the model package
```dart
import 'package:docsrv_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**files** | [**List<FileData>**](FileData.md) |  | [optional] [default to const []]
**inputs** | [**List<WorkflowInputResult>**](WorkflowInputResult.md) |  | [optional] [default to const []]
**ppkId** | **String** |  | [optional] 
**role** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


