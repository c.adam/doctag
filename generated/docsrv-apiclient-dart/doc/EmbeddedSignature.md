# docsrv_api.model.EmbeddedSignature

## Load the model package
```dart
import 'package:docsrv_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**files** | [**List<FileData>**](FileData.md) |  | [optional] [default to const []]
**signature** | [**Signature**](Signature.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


