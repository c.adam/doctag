# docsrv_api.model.Signature

## Load the model package
```dart
import 'package:docsrv_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**DoctagSignatureData**](DoctagSignatureData.md) |  | [optional] 
**inputs** | [**List<WorkflowInputResult>**](WorkflowInputResult.md) |  | [optional] [default to const []]
**originalMessage** | **String** |  | [optional] 
**role** | **String** |  | [optional] 
**signed** | [**DateTime**](DateTime.md) |  | [optional] 
**signedByKey** | [**PublicKeyResponse**](PublicKeyResponse.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


