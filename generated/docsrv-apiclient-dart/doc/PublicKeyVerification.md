# docsrv_api.model.PublicKeyVerification

## Load the model package
```dart
import 'package:docsrv_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isAddressVerified** | **bool** |  | [optional] 
**isSigningDoctagInstanceVerified** | **bool** |  | [optional] 
**signatureOfPublicKeyEntry** | **String** |  | [optional] 
**signatureValidUntil** | **String** |  | [optional] 
**signedAt** | **String** |  | [optional] 
**signedByParty** | **String** |  | [optional] 
**signedByPublicKey** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


