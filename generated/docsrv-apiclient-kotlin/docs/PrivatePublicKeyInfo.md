

# PrivatePublicKeyInfo


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ppkId** | **String** |  |  [optional]
**verboseName** | **String** |  |  [optional]



