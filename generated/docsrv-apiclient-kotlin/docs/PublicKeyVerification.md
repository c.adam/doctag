

# PublicKeyVerification


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isAddressVerified** | **Boolean** |  |  [optional]
**isSigningDoctagInstanceVerified** | **Boolean** |  |  [optional]
**signatureOfPublicKeyEntry** | **String** |  |  [optional]
**signatureValidUntil** | **String** |  |  [optional]
**signedAt** | **String** |  |  [optional]
**signedByParty** | **String** |  |  [optional]
**signedByPublicKey** | **String** |  |  [optional]



