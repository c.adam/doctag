

# WorkflowInput


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  |  [optional]
**kind** | [**KindEnum**](#KindEnum) |  |  [optional]
**name** | **String** |  |  [optional]
**options** | [**WorkflowInputOptions**](WorkflowInputOptions.md) |  |  [optional]



## Enum: KindEnum

Name | Value
---- | -----
TEXTINPUT | &quot;TextInput&quot;
FILEINPUT | &quot;FileInput&quot;
SELECTFROMLIST | &quot;SelectFromList&quot;
CHECKBOX | &quot;Checkbox&quot;
SIGN | &quot;Sign&quot;



