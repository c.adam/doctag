

# SignatureInputs


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**files** | [**List&lt;FileData&gt;**](FileData.md) |  |  [optional]
**inputs** | [**List&lt;WorkflowInputResult&gt;**](WorkflowInputResult.md) |  |  [optional]
**ppkId** | **String** |  |  [optional]
**role** | **String** |  |  [optional]



