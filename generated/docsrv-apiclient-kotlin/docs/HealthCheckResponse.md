

# HealthCheckResponse


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isHealthy** | **Boolean** |  |  [optional]



