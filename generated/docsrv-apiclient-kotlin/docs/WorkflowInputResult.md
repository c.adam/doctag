

# WorkflowInputResult


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileId** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**value** | **String** |  |  [optional]



