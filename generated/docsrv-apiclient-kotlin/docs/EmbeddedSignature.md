

# EmbeddedSignature


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**files** | [**List&lt;FileData&gt;**](FileData.md) |  |  [optional]
**signature** | [**Signature**](Signature.md) |  |  [optional]



