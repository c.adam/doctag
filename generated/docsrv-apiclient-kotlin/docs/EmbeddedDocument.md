

# EmbeddedDocument


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document** | [**Document**](Document.md) |  |  [optional]
**files** | [**List&lt;FileData&gt;**](FileData.md) |  |  [optional]



