

# ZonedDateTime


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dateTime** | **OffsetDateTime** |  |  [optional]
**offset** | [**ZoneOffset**](ZoneOffset.md) |  |  [optional]
**zone** | **Object** |  |  [optional]



