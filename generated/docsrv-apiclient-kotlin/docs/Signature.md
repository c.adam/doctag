

# Signature


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**DoctagSignatureData**](DoctagSignatureData.md) |  |  [optional]
**inputs** | [**List&lt;WorkflowInputResult&gt;**](WorkflowInputResult.md) |  |  [optional]
**originalMessage** | **String** |  |  [optional]
**role** | **String** |  |  [optional]
**signed** | **OffsetDateTime** |  |  [optional]
**signedByKey** | [**PublicKeyResponse**](PublicKeyResponse.md) |  |  [optional]



