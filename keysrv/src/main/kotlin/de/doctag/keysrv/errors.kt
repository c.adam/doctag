package de.doctag.keysrv

import java.lang.Exception

class BadRequest(message: String) : Exception(message)